/**
 * Created by Thomas on 15-12-2015.
 */
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true}));
var mysql = require('../db/mysql');

router.get('/images/', function(req, res, next) {
    mysql.queryAllImages(res, imagesToJSON, Number.NEGATIVE_INFINITY);
});

router.get('/images/:number', function(req, res, next) {
    mysql.queryImagesByNumber(res, imagesToJSON, Number.NEGATIVE_INFINITY, req.params.number);
});

router.get('/images/minConf/:conf', function(req, res, next) {
    mysql.queryAllImages(res, imagesToJSON, req.params.conf);
});

router.get('/images/:number/minConf/:conf', function(req, res, next) {
    mysql.queryImagesByNumber(res, imagesToJSON, req.params.conf, req.params.number);
});

router.post('/images/add/', function(req, res) {
    var number = req.body.number;
    var url = req.body.url;
    mysql.addImage(number, url);
    console.log("POST /images/add/ for " + number + " with " + url);
});

router.post('/images/incpos/', function(req, res) {
    var ref = req.body.ref;
    mysql.incrementPosCount(ref);
    console.log("POST /images/incpos/ for " + ref);
});

router.post('/images/incneg/', function(req, res) {
   var ref = req.body.ref;
    mysql.incrementNegCount(ref);
    console.log("POST /images/incneg/ for " + ref);
});

function imagesToJSON(res, data, minConf) {
    var fdata = {
        "result" : []
    };
    for (var i in data) {
        var row = data[i];
        var crow = computeConfidence(row);
        var frow = {
            "number" : crow[0],
            "url" : crow[1],
            "posCount" : crow[2],
            "negCount" : crow[3],
            "conf" : crow[4]
        };
        if (frow.conf >= minConf) {
            fdata.result.push(frow);
        }
    }
    res.json(fdata);
}

function computeConfidence(row) {
    var pos = parseInt(row.poscount);
    var neg = parseInt(row.negcount);
    var conf = (pos - neg) / (pos + neg + 1);
    return [row.number, row.url, row.poscount, row.negcount, conf];
}

module.exports = router;