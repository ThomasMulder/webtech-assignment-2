var express = require('express');
var router = express.Router();
var requests = require('../requests/requests');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {});
});

router.get('/favicon.ico', function(req, res, next) {});

router.get('/:number', function(req, res, next) {
  var number = req.params.number;
  requests.getImageData(res, updateLocal, updateGoogle, number);
});

function updateLocal(res, data) {
    var number = data[0].number;
    var general = data[0].text;
    var math = data[1].text;
    var year = data[2].text;
    var results = data[3].result;
    var mostConfident = getNMostConfident(results, 5);
    var index = parseInt(mostConfident.length * Math.random());
    var link = mostConfident[index].url;
    res.render('index', {number : number, trivia: general, year: year, math : math, ref : link});
}

function updateGoogle(res, data) {
    var number = data[0].number;
    var general = data[0].text;
    var math = data[1].text;
    var year = data[2].text;
    var items = data[3].items;
    var link = items[0].link;
    for (var i in items) {
        var item = items[i];
        requests.postImage(number, item.link);
    }
    res.render('index', {number : number, trivia: general, year: year, math : math, ref : link});
}

function getNMostConfident(images, n) {
    var result = [];
    while (n > 0) {
        var aux = {
            "number": 0,
            "url": "",
            "poscount": 0,
            "negcount": 0,
            "conf": 0
        };
        var index = -1;
        for (var i in images) {
            var image = images[i];
            if (aux.conf <= image.conf) {
                index = i;
                aux = image;
            }
        }
        if (!(aux.url === "")) {
            result.push(aux);
        }
        if (index > -1) {
            images.splice(index, 1);
        }
        n--;
    }
    return result;
}

module.exports = router;
