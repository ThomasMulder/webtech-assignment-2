/**
 * Created by Thomas on 15-12-2015.
 */
$(document).ready(function(){
    //$(".results").hide();
    // Bind function to keypress-event of the input field.
    $(".controls input").on("keypress", function(event) {
        var value = parseInt($(".controls input").val()); // Input field value, as integer.
        $(".error").fadeOut('fast');
        if (event.which == 13) { // Enter was pressed.
            if (isNaN(value)) { // Input is not a number.
                $(".error").fadeIn('fast');
                $(".error").html("Error: this is not a number!");
            } else { // Valid input.
                $(".results").fadeOut('fast');
                $(".results").fadeIn('slow');
                window.location.href = '/' + value;
            }
        }
    });

    $('#positive').on("click", function() {
        var ref = $('#positive').attr("ref");
        $.post("/api/images/incpos/", { ref : ref });
        $('.feedback').fadeOut();
    });
    $('#negative').on("click", function() {
       var ref = $('#negative').attr("ref");
        $.post("/api/images/incneg/", { ref : ref });
        $('.feedback').fadeOut();
    });
});