/**
 * Created by Thomas on 20-12-2015.
 */
var request = require('request');
var async = require('async');

request.getImageData = function(res, localCB, googleCB, number) {
    var localResources = true;
    async.parallel([
        function(callback) {
            request('http://numbersapi.com/' + number + '/trivia?json', function(err, resp, body) {
                if (!err && resp.statusCode == 200) {
                    callback(null, JSON.parse(body));
                }
            })
        },
        function(callback) {
            request('http://numbersapi.com/' + number + '/math?json', function(err, resp, body) {
                if (!err && resp.statusCode == 200) {
                    callback(null, JSON.parse(body));
                }
            })
        },
        function(callback) {
            request('http://numbersapi.com/' + number + '/year?json', function(err, resp, body) {
                if (!err && resp.statusCode == 200) {
                    callback(null, JSON.parse(body));
                }
            })
        },
        function(callback) {
            request('http://localhost:3000/api/images/' + number, function(err, resp, body) {
                if (!err && resp.statusCode == 200) {
                    var json = JSON.parse(body);
                    var result = json.result;
                    if (result.length === 0) {
                        localResources = false;
                        request('https://www.googleapis.com/customsearch/v1?key=AIzaSyAnX-ZsHwTCylCw-6s0aXsUL9Q7Dm57Ee8&cx=016272339351304049495:95bn3l0ja-g&q=' + number + '%20number&searchType=image', function(gerr, gresp, gbody) {
                            if (!gerr && gresp.statusCode == 200) {
                                callback(null, JSON.parse(gbody));
                            }
                        });
                    } else {
                        callback(null, json);
                    }
                }
            })
        }
    ], function(err, results) {
        if (localResources) {
            localCB(res, results);
        } else {
            googleCB(res, results);
        }
    });
};

request.postImage = function (number, url) {
    request.post('http://localhost:3000/api/images/add/', {form : {number : number, url : url}}, function(err){});
};

request.postIncrementPositive = function(ref) {
  request.post('http://localhost:3000/api/images/incPos/', {form : {ref : ref}}, function(err){});
};

request.postIncrementNegative = function(ref) {
    request.post('http://localhost:3000/api/images/incNeg/', {form : {ref : ref}}, function(err){});
};

module.exports = request;