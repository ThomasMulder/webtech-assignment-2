/**
 * Created by Thomas on 15-12-2015.
 */
var mysql = require('mysql');
var connection = mysql.createConnection({
    host:       'localhost',
    user:       'root',
    password:   '',
    database:   'numberdb'
});

connection.queryAllImages = function(res, callback, minConf) {
  connection.query("SELECT number, url, poscount, negcount FROM images LIMIT 0, 1000;", function(err, rows, fields) {
       if (err) throw err;
      callback(res, rows, minConf);
    });
};

connection.queryImagesByNumber = function(res, callback, minConf, number) {
    connection.query("SELECT number, url, poscount, negcount FROM images WHERE number = " + number + " LIMIT 0, 1000;",
        function(err, rows, fields) {
            if (err) throw err;
            callback(res, rows, minConf);
        });
};

connection.addImage = function(number, url) {
    connection.query("INSERT INTO images (number, url) VALUES ('" + number + "', '" + url + "');",
        function(err, rows, fields) {
            if (err) throw err;
        });
};

connection.incrementPosCount = function(url) {
    connection.query("UPDATE images SET poscount = poscount + 1 WHERE url = '" + url + "';",
        function(err, rows, fields) {
            if (err) throw err;
        });
};

connection.incrementNegCount = function(url) {
    connection.query("UPDATE images SET negcount = negcount + 1 WHERE url = '" + url + "';",
        function(err, rows, fields) {
            if (err) throw err;
        });
};

module.exports = connection;